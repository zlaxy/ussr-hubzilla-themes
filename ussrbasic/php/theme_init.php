<?php
require_once('view/php/theme_init.php');

head_add_css('/library/fork-awesome/css/fork-awesome.min.css');
head_add_css('/vendor/twbs/bootstrap/dist/css/bootstrap.min.css');
head_add_css('/library/bootstrap-tagsinput/bootstrap-tagsinput.css');
head_add_css('/view/css/bootstrap-red.css');
head_add_css('/library/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css');

head_add_js('/vendor/twbs/bootstrap/dist/js/bootstrap.bundle.min.js');
head_add_js('/library/bootbox/bootbox.min.js');
head_add_js('/library/bootstrap-tagsinput/bootstrap-tagsinput.js');
head_add_js('/library/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js');

$ussrbasic_mode = '';
$ussrbasic_navbar_mode = '';

if (local_channel()) {
	$ussrbasic_mode = ((get_pconfig(local_channel(), 'ussrbasic', 'dark_mode')) ? 'dark' : 'light');
	$ussrbasic_navbar_mode = ((get_pconfig(local_channel(), 'ussrbasic', 'navbar_dark_mode')) ? 'dark' : 'light');
}

if (App::$profile_uid) {
	$ussrbasic_mode = ((get_pconfig(App::$profile_uid, 'ussrbasic', 'dark_mode')) ? 'dark' : 'light');
	$ussrbasic_navbar_mode = ((get_pconfig(App::$profile_uid, 'ussrbasic', 'navbar_dark_mode')) ? 'dark' : 'light');
}

if (!$ussrbasic_mode) {
	$ussrbasic_mode = ((get_config('ussrbasic', 'dark_mode')) ? 'dark' : 'light');
	$ussrbasic_navbar_mode = ((get_config('ussrbasic', 'navbar_dark_mode')) ? 'dark' : 'light');
}

App::$page['color_mode'] = 'data-bs-theme="' . $ussrbasic_mode . '"';
App::$page['navbar_color_mode'] = (($ussrbasic_navbar_mode === 'dark') ? 'data-bs-theme="' . $ussrbasic_navbar_mode . '"' : '');
